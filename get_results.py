#!/usr/bin/env python3

"""Calculate the mean availability of the based on the results of the performed checks."""

import os
import logging
import argparse
import configparser

import MySQLdb

log = logging.getLogger(__name__)
CONF_FILE = "/etc/monitortest.conf"


def load_config():
    conf = configparser.RawConfigParser()
    defaults = {
        "mysql": {
            "db": "monitortest",
            "host": "localhost",
            "user": "root",
            "password": "",
        },
    }
    for section, values in defaults.items():
        conf.add_section(section)
        for key, value in values.items():
            conf.set(section, key, value)

    # Override with local values
    if os.path.exists(CONF_FILE):
        conf.read(CONF_FILE)
    return conf


CONF = load_config()

# Instead of counting un/succesful results, do an
# average of all results while using 100 if it was available
# and 0 if not.
QUERY = """
SELECT
       a.id,
       a.api_call,
       AVG(
         IF(
             r.error_type IS NULL AND FLOOR(r.code / 100) = 2,
             100,
             0
          )
        ) as average
FROM `api_calls` a INNER JOIN `results` r ON (a.id=r.api_id)
GROUP BY a.id
"""


def main():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-d", "--debug", action="store_true",
                        help="Enable debugging output", default=False)
    args = parser.parse_args()
    logging.basicConfig()

    log.setLevel(logging.CRITICAL)
    if args.debug:
        log.setLevel(logging.DEBUG)

    db = MySQLdb.connect(
        host=CONF.get("mysql", "host"),
        user=CONF.get("mysql", "user"),
        password=CONF.get("mysql", "password"),
        db=CONF.get("mysql", "db"),
    )
    c = db.cursor()
    c.execute(QUERY)
    print("ID\tAPI\tPercent Avail")
    for result in c.fetchall():
        print("%s\t%s\t%.2f%%" % result)


if __name__ == '__main__':
    main()
