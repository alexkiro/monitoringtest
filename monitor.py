"""Celery task and beat for monitoring endpoints.

Periodically check the endpoint and store the results in the db.
"""

import os
import datetime
import configparser

try:
    import MySQLdb
except ImportError:
    import pymysql as MySQLdb
import requests
import requests.exceptions

from celery import Celery
from celery.utils.log import get_task_logger

app = Celery("monitor_tasks", broker='redis://localhost:6379/0')
app.conf.beat_schedule = {
    'periodic-check-availability': {
        'task': 'monitor.periodic_check',
        'schedule': 300.0,  # Every X seconds
    },
}
app.conf.timezone = 'UTC'
log = get_task_logger("monitor_tasks")

CONF_FILE = "/etc/monitortest.conf"


def load_config():
    conf = configparser.RawConfigParser()
    defaults = {
        "mysql": {
            "db": "monitortest",
            "host": "localhost",
            "user": "root",
            "password": "",
        },
    }
    for section, values in defaults.items():
        conf.add_section(section)
        for key, value in values.items():
            conf.set(section, key, value)

    # Override with local values
    if os.path.exists(CONF_FILE):
        conf.read(CONF_FILE)
    return conf


CONF = load_config()


@app.task(bind=True)
def periodic_check(self):
    """Get the list of current API to check and schedule the
    actual check.
    """
    try:
        db = MySQLdb.connect(
            host=CONF.get("mysql", "host"),
            user=CONF.get("mysql", "user"),
            password=CONF.get("mysql", "password"),
            db=CONF.get("mysql", "db"),
        )
        c = db.cursor()
    except MySQLdb.Error as e:
        log.warning("Unable to connect to DB: %s", e)
        # Retry to ensure we perform the check
        raise self.retry(exc=e)

    try:
        c.execute("SELECT `id`, `api_call` FROM `api_calls`")
        results = c.fetchall()
    except MySQLdb.Error as e:
        log.warning("Unable to fetch results from DB: %s", e)
        # Retry to ensure we perform the check
        raise self.retry(exc=e)
    finally:
        c.close()
        db.close()

    for api_id, api_call in results:
        # Schedule the actual check in the celery queue.
        check_availability.delay(api_id, api_call)


@app.task
def check_availability(api_id, api_call):
    """Check the availability of the API call. Storing the result
    in the DB.
    """
    # Using stream=True, ensure that the response body is
    # not downloaded.
    error_type = None
    code = None

    try:
        response = requests.get(api_call, stream=True)
    except requests.exceptions.Timeout as e:
        log.info("Got timeout from api call %s: %s", api_call, e)
        error_type = "timeout"
    except requests.exceptions.ConnectionError as e:
        log.info("Got connection error from api call %s: %s", api_call, e)
        error_type = "connect"
    else:
        code = response.status_code
        log.info("Got response with headers %s", response.headers)
        response.close()

    # Use the TS here, instead of the auto DB one, in case
    # we need to retry the DB insert.
    ts = datetime.datetime.utcnow()
    store_results.delay(api_id, code, error_type, ts)


@app.task(bind=True)
def store_results(self, api_id, code, error_type, ts):
    """Store the results in the DB. This tasks retries
    if there are any temporary issues with the DB.
    """
    try:
        db = MySQLdb.connect(
            host=CONF.get("mysql", "host"),
            user=CONF.get("mysql", "user"),
            password=CONF.get("mysql", "password"),
            db=CONF.get("mysql", "db"),
        )
        c = db.cursor()
    except MySQLdb.Error as e:
        log.warning("Unable to connect to DB: %s", e)
        # Retry to ensure we perform the check
        raise self.retry(exc=e)

    try:
        c.execute("INSERT INTO results (`api_id`, `code`, `error_type`, `ts`) "
                  "VALUES (%s, %s, %s, %s)", (api_id, code, error_type, ts))
        db.commit()
    except MySQLdb.Error as e:
        log.warning("Unable to insert results from DB: %s", e)
        # Retry to ensure we perform the check
        raise self.retry(exc=e)
    finally:
        c.close()
        db.close()

