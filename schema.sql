CREATE DATABASE IF NOT EXISTS `monitortest`;

CREATE TABLE IF NOT EXISTS `monitortest`.`api_calls` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `api_call` varchar(2000) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB COMMENT 'API calls to be checked';

CREATE TABLE IF NOT EXISTS `monitortest`.`results` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `api_id` bigint(20) unsigned NOT NULL,
  `ts` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `code` int(3) unsigned,
  `error_type` varchar(50),
  CONSTRAINT fk_api_id FOREIGN KEY (api_id)
    REFERENCES `monitortest`.`api_calls` (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  PRIMARY KEY (id)
) COMMENT 'Monitoring result';


INSERT INTO `monitortest`.`api_calls` (api_call)
VALUES
  ("https://gis.tirol.gv.at/inspire/downloadservice/DownloadServiceFeed.xml"),
  ("http://gis.tirol.gv.at/inspire/downloadservice/Natura2000_FFH_Richtlinie_ETRS89UTM32N.zip"),
  ("http://geoportal.ibgebim.be/pdf/gml/Protectedsites.n2k_habitat.gml"),
  ("http://wwwd3.ymparisto.fi/d3/gis_data/gml/PS_Natura2000_3035.gml"),
  ("http://gmlid.eu/RO/ENV/PADS/PS/ROSCI0135")