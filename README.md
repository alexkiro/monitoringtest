# monitoringtest


### Installing 

Install python requirments:
 
```
pip install -r requirements.txt
``` 

Note this requires `mysqlclient` library which in turn requires 
`libmariadbclient-dev` or `libmysqldbclient-dev` to be built. Alternatively 
simply install `pymysql` (pure python) as the code will automatically pick 
this library up instead. 

Create the required schema:

```
mysql < schema.sql
```  

Create the configuration file `/etc/monitortest.conf`:

```
[mysql]
password = XXXXXX
```


### Starting celery app worker and scheduler

This command will start both the worker and the beat scheduler

```
celery -A monitor worker --beat --loglevel=info
```

For daemonizing see Celery documentation [here](http://docs.celeryproject.org/en/latest/userguide/daemonizing.html)

### Adding new checks

In order to add new item to the list of checked API simply insert them into
the `api_calls` table. Example:

```
INSERT INTO `monitortest`.`api_calls` (api_call)
VALUES
  ("https://gis.tirol.gv.at/inspire/downloadservice/DownloadServiceFeed.xml"),
```

### Getting the results 

In order to view the average availability results simply run the `get_results.py`
script.

Example: 

```
$ python get_results.py 
ID	API	Percent Avail
1	https://gis.tirol.gv.at/inspire/downloadservice/DownloadServiceFeed.xml	96.60%
2	http://gis.tirol.gv.at/inspire/downloadservice/Natura2000_FFH_Richtlinie_ETRS89UTM32N.zip	96.60%
3	http://geoportal.ibgebim.be/pdf/gml/Protectedsites.n2k_habitat.gml	96.60%
4	http://wwwd3.ymparisto.fi/d3/gis_data/gml/PS_Natura2000_3035.gml	96.60%
5	http://gmlid.eu/RO/ENV/PADS/PS/ROSCI0135	96.60%
```
 
